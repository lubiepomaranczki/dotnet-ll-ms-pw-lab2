﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;


namespace LWS
{
    class log
    {
        public const string nazwaLog = "szpieg.log";

        public static void logarytm()
        {
            string doLogu = "";

            DateTime aktGodzina = DateTime.Now;

            doLogu = ("O godzinie:" + aktGodzina.ToString() + " wysłano obraz z " + obsObrazu.url + " na: " + wyslMail.adMail + " tag:" + obsObrazu.tag);

            using (FileStream fs = new FileStream(nazwaLog, FileMode.Create | FileMode.Append))
            {
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(doLogu);
                fs.Flush();
                sw.Close();
                fs.Close();
            }
        }
    }
}
