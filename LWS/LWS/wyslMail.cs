﻿/*
 * Wysyłanie maila pięknie działa, nie ruszaj. Ew. dodaj log! 
 * 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace LWS
{
    public static class wyslMail
    {
        public static string adMail;

        private static MailAddress fromAddress = new MailAddress("ala.makota.ipsa.ikonia@gmail.com");
        private static MailAddress toAdress = new MailAddress("adres@cos.com");
        private static string fromPassword = "dotnetjawa";
        private static string subject = "Twój obrazek na dziś";
        private static string body = "W załączniku wybrany obrazek";
        private static System.Net.Mail.Attachment zal;

        private static SmtpClient smtp = new SmtpClient
        {
            Host = "smtp.gmail.com",
            Port = 587,
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
        };

        public static void slij()
        {
            zal = new System.Net.Mail.Attachment("obrazek.jpg");
            toAdress = new MailAddress(adMail);
            MailMessage mail = new MailMessage();
            mail.From = fromAddress;
            mail.To.Add(toAdress);
            mail.Subject = subject;
            mail.Body = body;
            mail.Attachments.Add(zal);

            smtp.Send(mail);

            zal.Dispose();
        }
        
    }
}
