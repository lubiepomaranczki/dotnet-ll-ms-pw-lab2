﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Drawing;


namespace LWS
{
    class obsObrazu
    {
        public static string url;
        public static string tag;
        
        private static string GetPageHtml()
        {
            using (WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(url);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));

                return html;
            }
        }

        public static void pobierzOb(string url)
        {
            WebClient myWebClient = new WebClient();
            
            string filename = "obrazek.jpg";
            myWebClient.DownloadFile(url, filename);
            
        }

        public static bool znajdzOb()
        {

            HtmlDocument doc = new HtmlDocument();
            string pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").ToString().Contains(tag))
                {
                    pobierzOb(node.GetAttributeValue("src", ""));
                    return true;
                }
            }
            
            return false;
        }

    }
}
